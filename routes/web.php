<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\InfCommuncationController;

use App\Models\SubCategory;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

define('pag', 20);


/*Route::get('admin', function () {
    return view('adminsfront.admincategory');
})->middleware('auth:admin');
*/


Route::group([
    'middleware' => ['auth','verified'],
    'prefix' => 'category'
], function () {

    Route::get('/',[CategoryController::class,'all'])->name('all.category');

    Route::get('edit/{id}',[CategoryController::class,'edit'])->name('edit.category');

    Route::get('inforamtion/{id}',[CategoryController::class,'show'])->name('show.category');

    Route::get('create',[CategoryController::class,'create'])->name('create.category');
    Route::post('update/{id}',[CategoryController::class,'update'])->name('update.category');
    Route::post('delete', [CategoryController::class,'destroy'])->name('delete.category');

    

    Route::post('addCategory', [CategoryController::class,'store'])->name('add.category');
    /**
     * category
     * * */
});
/***********Sub Category******************/ 
Route::group([
    'middleware' => ['auth','verified'],
    'prefix' => 'subcategory'
], function () {


Route::get('edit/{id}',[SubCategoryController::class,'edit'])->name('edit.subcategory');
    Route::get('create/{id}',[SubCategoryController::class,'create'])->name('create.subcategory');
    
    Route::post('update/{id}',[SubCategoryController::class,'update'])->name('update.subcategory');
    
    Route::post('delete', [SubCategoryController::class,'destroy'])->name('delete.subcategory');

    

    Route::post('addsubCategory/{id}', [SubCategoryController::class,'store'])->name('add.subcategory');
    /**
     * category
     * * */
});
/**********information************* */
Route::group([
    'middleware' => ['auth','verified'],
    'prefix' => 'informationcommuncation'
], function () {

    
    Route::get('/',[InfCommuncationController::class,'all'])->name('all.information');

    Route::get('edit/{id}',[InfCommuncationController::class,'edit'])->name('edit.information');

    Route::get('inforamtion/{id}',[InfCommuncationController::class,'show'])->name('show.information');

    Route::get('create',[InfCommuncationController::class,'create'])->name('create.information');
    Route::post('update/{id}',[InfCommuncationController::class,'update'])->name('update.information');
    Route::post('delete', [InfCommuncationController::class,'destroy'])->name('delete.information');

    Route::post('addinformation', [InfCommuncationController::class,'store'])->name('add.information');
    
    
});

/*********user*****************/ 
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {

        Route::get('showSub/{id?}',[SubCategoryController::class,'showSub'])->name('showSub.subcategory');


        Route::get('/', [CategoryController::class,'index'])->name('index');
        Route::get('/construction' , [UserController::class,'construction'])->name('construction');
        Route::get('/contact-us' , [UserController::class,'contact_us'])->name('contact-us');

        Route::get('categoryc/{id}',[UserController::class,'afterServices'])->name('afterServicesc');

        Route::get('/iadc-member' , [UserController::class,'iadc_member'])->name('iadc-member');

        Route::get('/iso' , [UserController::class,'iso'])->name('iso');

        Route::get('/oilandGas' , [UserController::class,'oilandGas'])->name('oilandGas');

        Route::get('/services' , [UserController::class,'services'])->name('services');
  
        Route::get('/carrer' , [UserController::class,'carrer'])->name('carrer');
        Route::get('/about_us' , [UserController::class,'about_us'])->name('about_us');
        Route::get('/enviroment' , [UserController::class,'enviroment'])->name('enviroment');
        
        Route::get('/afterServices' , [UserController::class,'afterServices'])->name('afterServices');
        
        
    }
);

Route::get('/Dashboard' , function (){
    return view ('adminDashboard');
})->name('Dashboard')->middleware(['auth','verified']);
Route::get('informationcomm', [InfCommuncationController::class,'index']);
Auth::routes(['verify'=>true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
