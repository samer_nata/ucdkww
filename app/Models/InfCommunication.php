<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfCommunication extends Model
{
    //use HasFactory;
    protected $table = 'informations';
    protected $fillable = ['title', 'details', 'type'];

    public function getIcon()
    {
        switch ($this->type) {
            case 'linkedin':
                return "fab fa-linkedin";

            case 'whatsapp':
                return "fab fa-whatsapp";

            case 'drive':
                return "fab fa-google-drive";

            case 'gmail':
                return "fab fa-google-plus";

            case 'mobile':
                return "fas fa-mobile-alt";

            case 'telephone':
                return "fas fa-tty";

                case 'facebook':
                    return "fab fa-facebook";
           
        }
    }
}
