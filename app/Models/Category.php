<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\SubCategory;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Category extends Model
{
    //use HasFactory;
    protected $table='categories';
    protected $fillable=['name_en','name_ar'];

   public function subCategories()
   {
       return $this->hasMany(SubCategory::class,'idc','id')->select(array('id','idc', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name', 'details_' . LaravelLocalization::getCurrentLocale() . ' as details','photos'));
   }

   public function subCategoriesa()
   {
       return $this->hasMany(SubCategory::class,'idc','id');
   }
}