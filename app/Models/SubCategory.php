<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use Illuminate\Support\Facades\File;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SubCategory extends Model
{
    //use HasFactory;
    protected $table = 'subcategories';
    //use HasFactory;
    protected $fillable = ['name_en', 'details_ar', 'details_en', 'name_ar', 'photos', 'idc'];
    public function category()
    {

        return    $this->hasone(Category::class, 'id','idc')->select(array('id', 'name_' . LaravelLocalization::getCurrentLocale() . ' as name'));
    }

    public function getphotos()
    { //return  json_decode($this->photos);
        $photos = [];
        $data = json_decode($this->photos);


        foreach ($data as $file) {
            if (File::exists(public_path('images/subcategories/' . $file))) {
                $photos[] = $file;
                /// File::delete(public_path('images/subcategories/' . $file));
            }
        }

        return $photos;
    }
}
