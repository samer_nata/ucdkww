<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfCommunicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required',
            'details'=>'required',
            'type'=>'required|in:linkedin,whatsapp,drive,gmail,mobile,telephone,facebook',
        ];
    }
}
