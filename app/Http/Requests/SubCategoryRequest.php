<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'name_en'=>'required|max:30',
            'name_ar'=>'required|max:30',
            'details_ar'=>'required',
            'details_en'=>'required',
           // 'idc'=>'required|numeric|exists:categories,id',
            'photos'=>'array',
           'photos.*'=>'mimes:png,jpeg,jpg|max:1024'
        ];
    }
}
