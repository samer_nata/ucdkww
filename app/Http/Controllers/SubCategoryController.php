<?php

namespace App\Http\Controllers;

use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Requests\SubCategoryRequest;
use App\Models\Category;
use App\Models\InfCommunication;
use App\Traits\SubCategoryTrait;
use Illuminate\Support\Facades\File;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SubCategoryController extends Controller
{
    use SubCategoryTrait;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SubCategory::get()->Paginate(pag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idc)
    {
        $cat =  Category::find($idc);
        if (!$cat) {
            return redirect()->back()->with(['success' => false, 'msg' => 'notfound']);
        }
        return view('Category.createsub', compact('cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request, $id)
    {
        // $data = array();
        /*
        //echo $request->photos;
        foreach ($request->photos as  $value) {
           echo $value;
        }

        return;*/
        $data = array();
        if ($request->hasfile('photos')) {
            foreach ($request->file('photos') as $file) {
                //  echo "filename".$file;
                //   $file_name = $this->saveImage($file, 'images/subcategories');
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/images/subcategories', $name);
                // array_push($data,$file_name);
                $data[] = $name;
            }
        }
        /*foreach ($data as $i) {
            echo $i;
        }

        return;*/
        $sub = SubCategory::create([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'details_ar' => $request->details_ar,
            'details_en' => $request->details_en,
            'idc' => $id,
            'photos' => json_encode($data)

        ]);
        if ($sub)
            return redirect()->back()->with([
                'success' => true,
                'msg' => 'تم الحفظ بنجاح',
            ]);

        else
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat =  SubCategory::find($id);
        if (!$cat) {
            return redirect()->back()->with(['notfound' => 'notfound']);
        }
        return $cat;
    }
    public function showSub($id = null)
    {
        $cats= Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $cat = SubCategory::select('id', 'name_'.LaravelLocalization::getCurrentLocale(). ' as name',
        'details_'.LaravelLocalization::getCurrentLocale(). ' as details','idc','photos'
        )->get();
        $sub =  SubCategory::select('id', 'name_'.LaravelLocalization::getCurrentLocale(). ' as name',
        'details_'.LaravelLocalization::getCurrentLocale(). ' as details','idc','photos'
        )->find($id);
        $infs=InfCommunication::get();
        return view("userpages.services" , compact('sub' , 'cat' ,'cats','infs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub =  SubCategory::find($id);
        if (!$sub) {
            return redirect()->back()->with(['notfound' => 'notfound']);
        } else {


            return view('Category.editsub', compact('sub'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, $id)
    {
        $sub =  SubCategory::find($id);
        if (!$sub) {
            return redirect()->back()->with(['notfound' => 'notfound']);;
        }
        $data = array();

        if ($request->hasfile('photos')) {
            foreach ($request->file('photos') as $file) {
                //  echo "filename".$file;
                //   $file_name = $this->saveImage($file, 'images/subcategories');
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/images/subcategories', $name);
                // array_push($data,$file_name);
                $data[] = $name;
            }
        }



        $sub->name_en = $request->name_en;
        $sub->name_ar = $request->name_ar;
        $sub->details_ar = $request->details_ar;
        $sub->details_en = $request->details_en;
        //$sub->idc = $request->idc;
        $sub->photos = json_encode($data);
        $sub->save();

        return  redirect()->back()->with(['success'=>true,'msg' => 'تم التحديث']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $cat =  SubCategory::find($request->id);
        if (!$cat) {
            // return  redirect()->back()->with(['error' => __('messages.not found offer')]);
            return response()->json(['status' => false, 'msg' => 'لم يتم الحفظ', 'id' => $request->id]);
        }
        /*
        foreach ($cat->getphotos() as $file) {

            if (File::exists(public_path('images/subcategories/' . $file))) {
                File::delete(public_path('images/subcategories/' . $file));
            }
        }*/


        $cat->delete();
        return response()->json(['status' => true, 'msg' => 'تم الحفظ بنجاح', 'id' => $request->id]);
    }
}
