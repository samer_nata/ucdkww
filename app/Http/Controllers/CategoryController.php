<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Models\InfCommunication;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class CategoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $cats= Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get(); 
        $infs=InfCommunication::get();
        $samer="samer";  
        
        
        return view('userpages.index',compact('cats' , 'samer','infs'));
    }
    public function all()
    {
        $categories = Category::get();

        return view('Category.allCategories', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $categoryrequest)
    {
        $cat = Category::create(['name_en' => $categoryrequest->name_en, 'name_ar' => $categoryrequest->name_ar]);
        if ($cat)
            return redirect()->back()->with([
                'success' => true,
                'msg' => 'تم الحفظ بنجاح',
            ]);

        else
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat =  Category::find($id);
        if (!$cat) {
            return redirect()->back()->with(['success' => false,'msg'=>'notfound']);
        }
        
        return view('Category.subcategory',compact('cat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);
        if (!$cat) {
            return redirect()->back();
        }
        return view('Category.edit', compact('cat'));
        //  $offer = Category::select('id', 'name', 'price', 'details')->find($id);
        //return view('offers.edit', compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request,$id)
    {
        
        $cat =  Category::find($id);
        if (!$cat) {
            return redirect()->back()->with(['success' => false, 'msg' => 'لم يتم الحفظ', 'id' => $request->id]);
        }
        $cat->update($request->all());

        return redirect()->back()->with(['success' => true, 'msg' => 'تم الحفظ بنجاح', 'id' => $request->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $cat =  Category::find($request->id);
        if (!$cat) {
            // return  redirect()->back()->with(['error' => __('messages.not found offer')]);
            return response()->json(['status' => false, 'msg' => 'لم يتم الحفظ', 'id' => $request->id]);
        }
        $cat->delete();
        return response()->json(['status' => true, 'msg' => 'تم الحفظ بنجاح', 'id' => $request->id]);
    }
}
