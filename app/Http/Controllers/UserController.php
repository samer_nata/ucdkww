<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use App\Models\InfCommunication;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class UserController extends Controller
{

    public function construction()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.construction", compact('cats','infs'));
    }

    public function contact_us()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.contact-us", compact('cats','infs'));
    }
    public function iadc_member()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.iadc-member", compact('cats','infs'));
    }

    public function iso()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.iso", compact('cats','infs'));
    }
    public function oilandGas()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.oilandGas", compact('cats','infs'));
    }
    public function services()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.services", compact('cats','infs'));
    }

    
    public function carrer()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.carrer", compact('cats','infs'));
    }
    public function about_us()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.About-us", compact('cats','infs'));
    }
    public function enviroment()
    {
        $cats = Category::select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();
        $infs=InfCommunication::get();
        return view("userpages.enviroment", compact('cats','infs'));
    }

    public function afterServices($id)
    { 
        $cats= Category::with('subCategories')->select(
            'id',
            'name_' . LaravelLocalization::getCurrentLocale() . ' as name',
        )->get();

        
        $cat= $cats->where('id', $id)->first();
        $infs=InfCommunication::get();
       
        return view("userpages.afterServices" , compact( 'cat' ,'cats','infs'));
      //  return view("userpages.afterServices", compact('cats','cat'));
    }
}
