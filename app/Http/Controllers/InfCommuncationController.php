<?php

namespace App\Http\Controllers;

use App\Http\Requests\InfCommunicationRequest;
use App\Models\InfCommunication;
use Illuminate\Http\Request;

class InfCommuncationController extends Controller
{
    public function all()
    {
        $infs = InfCommunication::get();

        return view('InfCommuncation.allCommuncation', compact('infs'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InfCommunication::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('InfCommuncation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $inf = InfCommunication::create([
            'title' => $request->title,
            'details' => $request->details,
            'type' => $request->type
        ]);
        if ($inf)
            return redirect()->back()->with([
                'success' => true,
                'msg' => 'تم الحفظ بنجاح',
            ]);

        else
            return redirect()->back()->with([
                'success' => false,
                'msg' => 'فشل الحفظ برجاء المحاوله مجددا',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inf =  InfCommunication::find($id);
        if (!$inf) {
            return redirect()->back()->with(['success' => false, 'msg' => 'notfound']);
        }
        // return view('Category.subcategory',compact('cat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inf =  InfCommunication::find($id);
        if (!$inf) {
            return redirect()->back()->with(['notfound' => 'notfound']);;
        } else {

            return view('InfCommuncation.edit', compact('inf'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InfCommunicationRequest $request, $id)
    {
        $inf =  InfCommunication::find($id);
        if (!$inf) {
            return redirect()->back()->with(['success' => false, 'msg' => 'لم يتم الحفظ', 'id' => $request->id]);
        }
        $inf->update($request->all());

        return redirect()->back()->with(['success' => true, 'msg' => 'تم الحفظ بنجاح', 'id' => $request->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $inf =  InfCommunication::find($request->id);
        if (!$inf) {
            // return  redirect()->back()->with(['error' => __('messages.not found offer')]);
            return response()->json(['status' => false, 'msg' => 'لم يتم الحفظ', 'id' => $request->id]);
        }
        $inf->delete();
        return response()->json(['status' => true, 'msg' => 'تم الحفظ بنجاح', 'id' => $request->id]);
    }
}
