<nav>
    <div class="logo">
        <img src="{{ asset('images/logo.png') }}" alt=""></a>
    </div>
    <label for="btn" class="icon">
        <i class="fa fa-bars"></i>
    </label>
    <input type="checkbox" id="btn">
    <ul>
        <li><a href="{{route('index')}}">{{ __('header.Home') }}</a></li>
        <li><a href="{{ route('about_us')}}">
            {{ __('header.About Us') }}</a></li>
        <li>
            <label for="btn-2" class="show">{{ __('header.Our Services') }} +</label>
            <a href="#">{{ __('header.Our Services') }}</a>
            <input type="checkbox" id="btn-2">
            <ul>
                @foreach ($cats as $item )
                <li>
                    <label for="btn-3" class="show">{{ $item-> name }} +</label>
                    <a href="{{route('afterServicesc',[$item->id])}}">{{ $item-> name }} <span class="fa fa-plus"></span></a>
                    <input type="checkbox" id="btn-3">
                    <ul>
                        @foreach ( $item->subCategories as $subcategory )
                        <li><a href="{{route('showSub.subcategory',[$subcategory ->id])}}">
                                {{$subcategory -> name}}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </li>
        <li>
            <label for="btn-1" class="show">{{ __('header.Equipment') }} +</label>
            <a href="#">{{ __('header.Equipment') }}</a>
            <input type="checkbox" id="btn-1">
            <ul>
                <li><a href="{{route('oilandGas')}}">{{ __('header.OilAndGas') }}</a></li>
                <li><a href="{{route('construction')}}">{{__('header.Construction')}}</a></li>
            </ul>
        </li>

        <li><a href="{{route('carrer')}}">{{ __('header.Carrer') }}</a></li>
        <li><a href="{{route('contact-us')}}">{{ __('header.Contact Us') }}</a></li>
      
        <li>
            <label for="btn-1" class="show">{{ __('header.Language') }} +</label>
            <a href="#">{{ __('header.Language') }}</a>
            <input type="checkbox" id="btn-1">  
            <ul class="language">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li>
                    <a class="btn btn-link"
                        href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                        {{ $properties['native'] }}</a>
                </li>
                @endforeach
            </ul>
        </li>
    </ul>
</nav>

