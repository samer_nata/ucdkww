<section id="footer-sec">
        <footer>
            <div class="footer-cont">
                <div class="sec aboutus">
                    <h2>{{ ('footer.About Us') }}</h2>
                    <p>
                        {{ ('footer.About Us Content') }}
                    </p>
                    <ul class="sci">
                       @foreach ($infs as $info)
                       <li>
                       <a href="{{$info->title}}"><i class="{{$info->getIcon()}}" aria-hidden="true"></i></a>
                        </li>
                       @endforeach
                       
                        
                       
                    </ul>
                </div>
                <div class="sec quickLinks">
                    <h2> {{ ('footer.Quick Links') }}</h2>
                    <ul>
                        <li><a href="{{route('index')}}">{{ ('footer.Home') }}</a></li>
                        <li><a href="{{route('about_us')}}">{{ ('footer.About Us') }}</a></li>
                        <li><a href="{{route('oilandGas')}}">{{ ('footer.Equipment') }}</a></li>
                        <li><a href="{{route('carrer')}}">{{ ('footer.Carrer') }}</a></li>
                        <li><a href="{{route('contact-us')}}">{{ ('footer.Contact Us') }}</a></li>
                        <li><a href="{{route('enviroment')}}">{{ ('footer.Enviroment') }}</a></li>
                        <li><a target="_blank" href="https://drive.google.com/file/d/1eLDUZChYCVeaeHOuEiJOWUcLoirEk4EG/view?usp=sharing">{{ ('header.profile') }}</a></li>
                    </ul>
                </div>
                <div class="sec contact">
                    <h2>{{ ('footer.Contact Info') }}</h2>
                    <ul class="info">
                        <li>
                            <span><i class="fas fa-map-marker-alt"></i></span>
                            <span>{{ ('footer.Contact Details') }} <br> {{__('footer.Contact Details2')}}</span>
                        </li>
                        <li>
                            <span><i class="fas fa-phone-alt"></i></span>
                            <p><a  href="#">+965 6511 1158</a><br>
                            </p>
                        </li>
                        <li class="logos">
                            <a href="{{route('iadc-member')}}"><img src="{{ asset('images/IADC-MEMBER.png') }}" alt=""></a> 
                            <a href="{{route('iso')}}"><img src="{{ asset('images/ISO.png') }}" alt=""></a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </footer>
        <div class="copyrightText">
            <p>Copyright © 2021 UCD. All Right Reserved | Designed and Developed with all love <a class="our-logo" href="https://wa.me/963958900716">VPC</a>
            </p>
        </div>
    </section>