@extends('layouts.app')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif
<!-- <div class="form-group">
    <label for="exampleInputEmail1">name_en</label>
    <label for="exampleInputEmail1">{{$cat->name_en}}</label>
    <label for="exampleInputEmail1">name_ar</label>
    <label for="exampleInputEmail1">{{$cat->name_ar}}</label>
  </div> -->
  <h1 Class="cat-title text-center">Category</h1>
  <div class="show-box">
    <span><b>Name En : </b>{{$cat->name_en}}</span>
    <span><b>Name Ar : </b>{{$cat->name_ar}}</span>
  </div>
  <h1 Class="cat-title text-center mb-4">Sub Category</h1>

 
<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">name_ar</th>
      <th scope="col">name_en</th>
      <th scope="col">operation</th>
    </tr>
  </thead>
  <tbody>

    @if(isset($cat))
    @foreach ($cat->subCategoriesa as $item )
    <tr class="subRow{{$item ->id}}">
      <th scope="row">{{$item ->id}}</th>
      <td>{{$item ->name_ar}}</td>
      <td>{{$item ->name_en}}</td>



      <td class="operation">
        <a href="{{route('edit.subcategory',[$item ->id])}}" class="btn btn-success"><i class="fas fa-edit"></i></button>
          @csrf
        <a href="" id="{{$item ->id}}" class="delete_btn btn btn-outline-danger"><i class="fas fa-trash"></i></button>
      </td>
    
    </tr>
    @endforeach

    @endif




  </tbody>
</table>
<form method="GET" action="{{Route('create.subcategory',[$cat->id])}}">
  @csrf
  <a href="{{route('all.category')}}" class="btn btn-outline-secondary" role="button"><i class="fas fa-arrow-left"></i></a>
  <button type="submit" class="btn btn-primary">Add SubCategory</button>
</form>

<br>  
@stop

@section('scripts')
<script>
  $(document).on('click', '.delete_btn', function(e) {
    e.preventDefault();

    var id = $(this).attr('id');
    $.ajax({
      type: "POST",
      //enctype: 'multipart/form-data',
      url: "{{route('delete.subcategory')}}",
      data: {
        '_token': "{{csrf_token()}}",
        'id': id
      },

      timeout: 600000,
      success: function(data) {



        if (data.status == true) {
          $('.subRow' + id).remove();
          //  $('#success_msg').show();


          /*var x = document.getElementById("success_msg");
          if (x.style.display === "none") {
            x.style.display = "block";
          }*/

        }
        $('.subRow' + data.id).remove();

      },
      error: function(e) {

        $("#result").text(e.responseText);
        console.log("ERROR : ", e);
        $("#btnSubmit").prop("disabled", false);

      }
    });
  });
</script>
@stop