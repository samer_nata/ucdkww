@extends('layouts.app')

@section('content')

@if (Session::has('success')&&Session('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif


<div class="row justify-content-center">
    <div class="col-lg-5 col-md-6 col-sm-12">

      <form method="POST" class="form" action="{{route('update.subcategory',[$sub->id])}}" enctype="multipart/form-data">
        @csrf



        <div class="form-group">
          <label for="exampleInputEmail1">name_en</label>
          <input type="text" name="name_en" class="form-control" value="{{$sub->name_en}}" placeholder="Name_En" required>
          @error('name_en')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>


        <div class="form-group">
          <label for="exampleInputEmail1">name_ar</label>
          <input type="text" name="name_ar" value="{{$sub->name_ar}}" class="form-control" placeholder="Name_Ar" required>
          @error('name_ar')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>

        <!--      --->

        <div class="form-group">
          <label for="exampleInputEmail1">name_ar</label>
          <input type="text" name="details_ar" value="{{$sub->details_ar}}" class="form-control" placeholder="details_ar" required>
          @error('details_ar')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">details_en</label>
          <input type="text" name="details_en" value="{{$sub->details_en}}" class="form-control" placeholder="details_en" required>
          @error('details_en')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>
      
        @foreach ($sub->getphotos() as $i )
        <div class="form-group" id="d{{pathinfo($i, PATHINFO_FILENAME)}}">
      <!-- <button type="button" id="{{pathinfo($i, PATHINFO_FILENAME)}}" class="btn.im btn-outline-danger">Delete</button> -->
        
          <!-- <input type="hidden" name="ol[]" value="{{$i}}"> -->
          <img id="im{{pathinfo($i, PATHINFO_FILENAME)}}"   src="/images/subcategories/{{$i}}" alt="..." class="img-thumbnail">
        </div>
        @endforeach

        <div class="container">
          <input type="file" name="photos[]" class="form-control" multiple id="gallery-photo-add">
          <div class="gallery"></div>
        </div>



        <button type="submit" class="btn btn-primary">Save</button>
      </form>
<br>
<a href="{{route('show.category',[$sub->idc])}}" class="btn btn-info" role="button">Back</a>

@stop

@section('scripts')
<script>
  $(function() {
    $("button").click(function() {
     if($(this).hasClass("btn.im")){

      var id=$(this).attr('id');
     // alert(id);
      //$('#d'+id).hide();
      $('#d'+id).remove();
     }
    var str = $(this).text();

  });
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

      if (input.files) {
        $("img").remove();
        var filesAmount = input.files.length;

        for (i = 0; i < filesAmount; i++) {
          var reader = new FileReader();

          reader.onload = function(event) {
            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
          }

          reader.readAsDataURL(input.files[i]);
        }
      }

    };

    $('#gallery-photo-add').on('change', function() {
      imagesPreview(this, 'div.gallery');
    });

  });
  
</script>
@stop