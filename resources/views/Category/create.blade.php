@extends('layouts.app')

@section('content')

@if (Session::has('success')&&Session('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif
<div class="row justify-content-center">
  <div class="col-lg-5 col-md-6 col-sm-12">
      <form method="POST" class="form" action="{{Route('add.category')}}">
      @csrf

      <div class="form-group">
        <label for="exampleInputEmail1">Name En</label>
        <input type="text" name="name_en" class="form-control form-control-lg"  placeholder="Name En" required>
        @error('name_en')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Name Ar</label>
        <input type="text" name="name_ar" class="form-control form-control-lg"  placeholder="Name Ar" required>
        @error('name_ar')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>
      <a href="{{route('all.category')}}" class="btn btn-outline-secondary btn-lg" role="button"><i class="fas fa-arrow-left"></i></a>
      <button type="submit" class="btn btn-primary btn-lg">Save</button>
    </form>
  </div>
</div>

<br>

@stop

@section('scripts')
<script>

</script>
@stop