@extends('layouts.app')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif

<div class="table-responsive-sm">
<table class="table table-hover table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">name_ar</th>
      <th scope="col">name_en</th>
      <th scope="col">operation</th>
    </tr>
  </thead>
  <tbody>

    @if(isset($categories)&&$categories->count()>0)
    @foreach ($categories as $item )
    <tr class="categoryRow{{$item ->id}}">
      <th scope="row">{{$item ->id}}</th>
      <td>{{$item ->name_ar}}</td>
      <td>{{$item ->name_en}}</td>

      <td class="operation">
        <a href="{{route('edit.category',[$item ->id])}}" class="btn btn-success"><i class="fas fa-edit"></i></button>
          @csrf
        <a href="" id="{{$item ->id}}" class="delete_btn btn btn-outline-danger"><i class="fas fa-trash"></i></button>
        <a href="{{route('show.category',[$item->id])}}" class="btn btn-info" role="button">Sub Category</a>
      </td>
    </tr>
    @endforeach

    @endif




  </tbody>
</table>
</div>

<a href="{{route('create.category')}}" class="btn btn-warning" role="button">Save</a>
<br>



@stop

@section('scripts')
<script>
  $(document).on('click', '.delete_btn', function(e) {
    e.preventDefault();

    var id = $(this).attr('id');
    $.ajax({
      type: "POST",
      //enctype: 'multipart/form-data',
      url: "{{route('delete.category')}}",
      data: {
        '_token': "{{csrf_token()}}",
        'id': id
      },

      timeout: 600000,
      success: function(data) {



        if (data.status == true) {
          $('.categoryRow' + id).remove();
          //  $('#success_msg').show();


          /*var x = document.getElementById("success_msg");
          if (x.style.display === "none") {
            x.style.display = "block";
          }*/

        }
        $('.categoryRow' + data.id).remove();

      },
      error: function(e) {

        $("#result").text(e.responseText);
        console.log("ERROR : ", e);
        $("#btnSubmit").prop("disabled", false);

      }
    });
  });
</script>
@stop