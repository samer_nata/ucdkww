@extends('layouts.app')

@section('content')

@if (Session::has('success')&&Session('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif

<div class="row justify-content-center">
  <div class="col-lg-5 col-md-6 col-sm-12">
    <form method="POST" class="form" action="{{Route('add.subcategory',[$cat->id])}}" enctype="multipart/form-data">
    @csrf

      <div class="form-group">
        <label for="exampleInputEmail1">Name En</label>
        <input type="text" name="name_en" class="form-control form-control-lg" placeholder="Name En" required>
        @error('name_en')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Name Ar</label>
        <input type="text" name="name_ar" class="form-control form-control-lg" placeholder="Name Ar" required>
        @error('name_ar')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Details En</label>
        <input type="text" name="details_en" class="form-control form-control-lg" placeholder="Details En" required>
        @error('details_en')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Details Ar</label>
        <input type="text" name="details_ar" class="form-control form-control-lg" placeholder="Details Ar" required>
        @error('details_ar')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>
      


      <div class="container">
      <label for="gallery-photo-add">Upload your image</label>
        <input type="file" name="photos[]" class="form-control-file"  multiple id="gallery-photo-add">
        @error('photos.*')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
        <div class="gallery"></div>
      </div>

      <a href="{{route('show.category',[$cat->id])}}" class="btn btn-outline-secondary btn-lg" role="button"><i class="fas fa-arrow-left"></i></a>
      <button type="submit" class="btn btn-warning btn-lg">Save</button>
    </form>
  </div>
</div>
<br>

@stop

@section('scripts')
<script>
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
          $( "img" ).remove( );
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>
@stop