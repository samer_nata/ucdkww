@extends('layouts.app')

@section('content')
    <div class="container">
    <h1 class="text-center">Dashboard</h1>

        <div class="row justify-content-evenly aling-items-center">
            <div class="col-6 p-5 d-flex justify-content-center">
                <a class="btn btn-lg btn-warning" href="{{route('all.category')}}">Category</a>
            </div>
            <div class="col-6 p-5">
                <a class="btn btn-lg btn-danger" href="{{route('all.information')}}">Information Communication</a>
            </div>
        </div>
    </div>

@endsection