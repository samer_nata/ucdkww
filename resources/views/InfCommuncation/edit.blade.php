@extends('layouts.app')

@section('content')

@if (Session::has('success')&&Session('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif
<div class="row justify-content-center">
    <div class="col-lg-5 col-md-6 col-sm-12">
      <form method="POST" class="form" action="{{Route('update.information',[$inf->id])}}">
        @csrf



        <div class="form-group">
          <label for="exampleInputEmail1">Title</label>
          <input type="text" name="title" value="{{$inf->title}}" class="form-control form-control-lg" placeholder="Title" required>
          @error('title')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>


        <div class="form-group">
          <label for="exampleInputEmail1">Details</label>
          <input type="text" name="details" value="{{$inf->details}}" class="form-control form-control-lg" placeholder="Details" required>
          @error('details')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>

        <div class="form-group">
          <label for="exampleInputEmail1">Type</label>
          <select class="custom-select custom-select-lg" name="type" aria-label="Default select example">
        
            <option value="linkedin" @if($inf->type == "linkedin") selected @endif>Linkedin</option>
            <option value="facebook" @if($inf->type == "facebook") selected @endif>Facebook</option>
            
            <option value="whatsapp" @if($inf->type == "whatsapp") selected @endif>Whatsapp</option>
            <option value="drive" @if($inf->type == "drive") selected @endif>Drive</option>
            <option value="gmail" @if($inf->type == "gmail") selected @endif>Gmail</option>
            <option value="mobile" @if($inf->type == "mobile") selected @endif>Mobile</option>
            <option value="telephone" @if($inf->type == "telephone") selected @endif>Telephone</option>
          </select>
          @error('type')
          <small class="form-text text-danger">{{$message}}</small>
          @enderror
        </div>


        <a href="{{route('all.information')}}" class="btn btn-outline-secondary btn-lg" role="button"><i class="fas fa-arrow-left"></i></a>
        
        
        <button type="submit" class="btn btn-warning btn-lg">Save</button>
      </form>
  </div>
</div>

<br>


@stop

@section('scripts')
<script>

</script>
@stop