@extends('layouts.app')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif
<div class="table-responsive-sm">
<table class="table table-hover table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">title</th>
      <th scope="col">details</th>

      <th scope="col">type</th>

      <th scope="col">operation</th>

     


    </tr>
  </thead>
  <tbody>

    @if(isset($infs)&&$infs->count()>0)
    @foreach ($infs as $item )
    <tr class="infsRow{{$item ->id}}">
      <th scope="row">{{$item ->id}}</th>
      <td>{{$item ->title}}</td>
      <td>{{$item ->details}}</td>
      <td>{{$item ->type}}</td>



      <td class="operation">
        <a href="{{route('edit.information',[$item ->id])}}" class="btn btn-success"><i class="fas fa-edit"></i></button>
          @csrf

        <a href="" id="{{$item ->id}}" class="delete_btn btn btn-danger"><i class="fas fa-trash"></i></button>
      </td>
      
    </tr>
    @endforeach

    @endif




  </tbody>
</table>
<a href="{{route('create.information')}}" class="btn btn-info" role="button">Add Information</a>
<br>



@stop

@section('scripts')
<script>
  $(document).on('click', '.delete_btn', function(e) {
    e.preventDefault();

    var id = $(this).attr('id');
    $.ajax({
      type: "POST",
      //enctype: 'multipart/form-data',
      url: "{{route('delete.information')}}",
      data: {
        '_token': "{{csrf_token()}}",
        'id': id
      },

      timeout: 600000,
      success: function(data) {



        if (data.status == true) {
          $('.infsRow' + id).remove();
          //  $('#success_msg').show();


          /*var x = document.getElementById("success_msg");
          if (x.style.display === "none") {
            x.style.display = "block";
          }*/

        }
        $('.infsRow' + data.id).remove();

      },
      error: function(e) {

        $("#result").text(e.responseText);
        console.log("ERROR : ", e);
        $("#btnSubmit").prop("disabled", false);

      }
    });
  });
</script>
@stop