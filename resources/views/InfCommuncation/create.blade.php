@extends('layouts.app')

@section('content')

@if (Session::has('success')&&Session('success'))
<div class="alert alert-success" role="alert">
  {{Session::get('msg')}}
</div>
@endif
<div class="row justify-content-center">
  <div class="col-lg-5 col-md-6 col-sm-12">
    <form method="POST" class="form" action="{{Route('add.information')}}">
      @csrf



      <div class="form-group">
        <label for="exampleInputEmail1">Title</label>
        <input type="text" name="title" class="form-control form-control-lg" placeholder="Title" required>
        @error('title')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>


      <div class="form-group">
        <label for="exampleInputEmail1">Details</label>
        <input type="text" name="details" class="form-control form-control-lg" placeholder="Details" required>
        @error('details')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Type</label>
        <select class="custom-select custom-select-lg" name="type" aria-label="Default select example">
          <option selected value="linkedin">Linkedin</option>
          <option value="whatsapp">Whatsapp</option>
          <option value="drive">Drive</option>
          <option value="gmail">Gmail</option>
          <option value="mobile">Mobile</option>
          <option value="telephone">Telephone</option>
          <option value="facebook">Facebook</option>
        </select>
        @error('type')
        <small class="form-text text-danger">{{$message}}</small>
        @enderror
      </div>

      <a href="{{route('all.information')}}" class="btn btn-outline-secondary" role="button"><i class="fas fa-arrow-left"></i></a>
      <button type="submit" class="btn btn-warning">Save</button>
    </form>
  </div>
</div>
<br>

@stop

@section('scripts')
<script>

</script>
@stop