@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content')

    <div class="abcontainer">
        <img src="{{asset('images/about-img.jpg')}}" alt="">

        <div class="about-details-container">
            <div class="aboutBx">
                <h2>{{__('aboutUs.About_us')}}</h2>
                <div class="box">{{__('aboutUs.abText1')}}</div>
                <div class="box">{{__('aboutUs.abText2')}}</div>
                <div class="box">{{__('aboutUs.abText3')}}</div>
                <div class="box">{{__('aboutUs.abText4')}}</div>
                <div class="box">{{__('aboutUs.abText5')}}</div>
            </div>
            <div class="inner-container">
                <div class="mission">
                    <h2>{{__('aboutUs.Misson')}}</h2>
                    <div class="box">{{__('aboutUs.MText1')}}</div>
                    <div class="box">{{__('aboutUs.abText2')}}</div>
                    <div class="box">{{__('aboutUs.abText3')}}</div>
                </div>
                <div class="vision">
                    <h2>{{__('aboutUs.vision')}}</h2>
                    <div class="box ">{{__('aboutUs.vText1')}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection