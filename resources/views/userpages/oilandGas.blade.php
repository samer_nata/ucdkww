@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content')

<div class="eq-container">
            
            <h1>2600 HP Oil Rig</h1>
            <img src="{{asset("images/Oil-Rig.jpg")}}" alt="">
            <h2>Details</h2>
            <div class="equipment-details">
                <div class="box">
                    <span><b>Rig Rated Depth:</b> 20,000 ft. (5” DP)</span>
                </div>
                <div class="box">
                    <span><b>Available Height of Derrick:</b> 45.46 m (149.1 ft.)</span>
                </div>
                <div class="box">
                    <span><b>Traveling System:</b> 6×7</span>
                </div>
                <div class="box">
                    <span><b>Diameter of Drilling Line:</b> 1-1/2” (38mm)</span>
                </div>
                <div class="box">
                    <span><b>Rig Rated Depth:</b> 20,000 ft. (5” DP)</span>
                </div>
                <div class="box">
                    <span><b>Height of Drilling Floor:</b> 11 m (36.08’ )</span>
                </div>

                <div class="box">
                    <span><b>Opening Diameter of Rotary Table:</b> 37-1/2” (952.5 mm)</span>
                </div>
                <div class="box">
                    <span><b>Rated Input Power of Draw Works:</b> 2600 HP (2000 kW)</span>
                </div>
                <div class="box">
                    <span><b>Top Drive: </b> Model; Varco TDS-11SA</span>
                </div>
                <div class="box">
                    <span><b>Capacity of Mud Pump:</b>1193 kW (1600 HP)×3 capacity </span>
                </div>
                <div class="box">
                    <span><b>Model and Quantity of Engine Power:</b> CAT 3512B x 4</span>
                </div>
                <div class="box">
                    <span><b>Three Shale Shaker:</b> Model; HS270-4P-PTS (4 panels), 7.2 G. capacity</span>
                </div>
                <div class="box">
                    <span><b>Two Mud Cleaner:</b> Model; HMC250X 3/100X16-270 (3x 10” Desander + 16 x 4” Diselter) capacity</span>
                </div>
                <div class="box">
                    <span><b>Two Centrifuge:</b>Model; LW355-1250N, variable speed, Max. Rotary speed: 3200 rpm</span>
                </div>
                <div class="box">
                    <span><b>Ambient Temperature:</b> -15℃ to 55℃ (dust/sand laden wind)</span>
                </div>
                <div class="box">
                    <span><b>Power Transfer Type:</b>AC, VFD</span>
                </div>
                <div class="box">
                    <span>4 centrifugal pumps for desander and desilter, model HCP 8× 6×14, 75 Kw, flow Capacity: 255 m3</span>
                </div>
                <div class="box">
                    <span>The rig Includes hydraulic skidding system and rail system, which can meet operation requirements of 20 wells in one line, well distance 7.5 m</span>
                </div>
                <div class="box">
                    <span>Hazardous area classification according to API RP 500 & Electrical Installation standards according to IEC</span>
                </div>
                <div class="box">
                    <span>5” and 3”1/2 DP, all sizes of DC, 5” and 3”1/2 HWDP, and all down hole equipment enough to drill to 20000 ft</span>
                </div>
                <div class="box">
                    <span>Handling tools to handle all type of tubular used on the rig </span>
                </div>
                <div class="box">
                    <span>Fishing tools to fish all types and sizes of tubular </span>
                </div>
                <div class="box">
                    <span>Well control equipment and all its accessories to drill 26” hole size and smaller details</span>
                </div>
                <div class="box">
                    <span>The substructure is prepared to accommodate the rolling gear system for future projects that require utilization feature</span>
                </div>
            </div>
        </div>

@endsection