@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])

@section('content')
<section>
    @foreach ($sub->getphotos() as $im )
    <img class="services-img" src="/images/subcategories/{{$im}}" alt="">
    @endforeach

    <h1 class="textServ">{{ $sub->category->name }}</h1>
    <div class="tabcontainer maxwidth">
        <div class="ux-vertical-tabs">
            <div class="tabs">
                @foreach ( $cat as $subcategory )
                @if ($subcategory->category->name == $sub->category->name )
                <button data-tab="tab{{$subcategory-> idc}}" @if ( $sub->idc == $subcategory-> idc)
                    class="active"
                    @endif
                    >{{ $subcategory-> name }} <span></span></button>
                @endif

                @endforeach
            </div>
            <div class="maincontent">
                @foreach ( $cat as $subcategory )
                @if ($subcategory->category->name == $sub->category->name)
                <div data-tab="tab{{$subcategory-> idc}}" class="tabcontent
                @if ($sub->idc == $subcategory-> idc)
                        active
                    @endif 
                ">
                    <div class="ux-text">
                        <h3> {{$subcategory -> name}}</h3>
                        <p>{{$subcategory -> details}}</p>

                    </div>
                </div>
                @endif

                @endforeach
            </div>

        </div>

    </div>
</section>

@endsection