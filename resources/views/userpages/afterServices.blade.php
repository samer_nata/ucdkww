@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])

@section('content')
<section>
    @foreach ($cat->subCategories as $sub )
    @foreach ($sub->getphotos() as $im )
    <img class="services-img" src="/images/subcategories/{{$im}}" alt="">
    @endforeach
    @endforeach



    <h1 class="textServ">{{ $cat->name }}</h1>
    <div class="tabcontainer maxwidth">
        <div class="ux-vertical-tabs">
            <div class="tabs">
                @foreach ( $cats as $ct )
                @if ($ct->name == $cat->name )
                <button data-tab="tab{{$ct-> id}}" @if ( $cat->id == $ct->id)
                    class="active"
                    @endif
                    >{{ $ct-> name }} <span></span></button>
                @endif

                @endforeach
            </div>
            

            <div class="maincontent">
                @foreach ( $cat->subCategories as $subcategory )
     
                <div data-tab="tab{{$subcategory-> idc}}" class="tabcontentactive">
                
                    <div class="ux-text">
                        
                        <p>{{$subcategory -> details}}</p>

                    </div>
                </div>
               

                @endforeach
            </div>

        </div>

    </div>
</section>

@endsection