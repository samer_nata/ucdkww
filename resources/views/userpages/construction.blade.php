@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content')

<div class="eq-container">
            
            <div class="img-container">
                <img src="{{asset("images/eq-1.jpg")}}"  alt="">
                <img src="{{asset("images/eq-2.jpg")}}" alt="">
                <img src="{{asset("images/eq-3.jpg")}}"  alt="">
                <img src="{{asset("images/eq-4.jpg")}}" class="img-lg" alt="">
                <img src="{{asset("images/eq-5.jpg")}}" alt="">
                <img src="{{asset("images/eq-6.jpg")}}" alt="">
                <img src="{{asset("images/eq-7.jpg")}}" class="img-lg" alt="">
                
            </div>
            <h2>Equipment</h2>
            <div class="equipment-details">
                <div class="box">
                    <span><b>1.</b> Motor Graders</span>
                </div>
                <div class="box">
                    <span><b>2.</b> Excavators (Tracked and Wheeled)</span>
                </div>
                <div class="box">
                    <span><b>3.</b> Wheel Loaders</span>
                </div>
                <div class="box">
                    <span><b>4.</b> Soil Stabilizer  </span>
                </div>
                <div class="box">
                    <span><b>5.</b> Bulldozers</span>
                </div>
                <div class="box">
                    <span><b>6.</b> Motor Scrapers</span>
                </div>

                <div class="box">
                    <span><b>7.</b> Soil Compactors and Rollers  </span>
                </div>
                <div class="box">
                    <span><b>8.</b> Pneumatic and Rotary Drills  </span>
                </div>
                <div class="box">
                    <span><b>9.</b> Compressors  </span>
                </div>
                <div class="box">
                    <span><b>10.</b> Asphalt Paving and Finishers  </span>
                </div>
                <div class="box">
                    <span><b>11.</b> Tippers   </span>
                </div>
                <div class="box">
                    <span><b>12.</b> Rock Dumpers (Articulated)</span>
                </div>
                <div class="box">
                    <span><b>13.</b> Aggregate Crushing Plants</span>
                </div>
                <div class="box">
                    <span><b>14.</b> Asphalt Batching and Mixing Plants</span>
                </div>
                <div class="box">
                    <span><b>15.</b> Concrete Batching and Mixing</span>
                </div>
                <div class="box">
                    <span><b>16.</b> Cranes (Mobile and Stationary)</span>
                </div>
                <div class="box">
                    <span><b>17.</b> Bitumen Sprayers and Distributors</span>
                </div>
                <div class="box">
                    <span><b>18.</b> Water Tankers</span>
                </div>
                <div class="box">
                    <span><b>19.</b> Piling Machine</span>
                </div>
                <div class="box">
                    <span><b>20.</b> Asphalt and Concrete Milling Machines</span>
                </div>
                <div class="box">
                    <span><b>21.</b> Concrete Pumps</span>
                </div>
                <div class="box">
                    <span><b>22.</b> Motorized Earth Leveler</span>
                </div>
                <div class="box">
                    <span><b>23.</b> Iron Molds</span>
                </div>
                <div class="box">
                    <span><b>24.</b>  Power Generation Units</span>
                </div>
                <div class="box">
                    <span><b>25.</b>  Air-Conditioning Production Workshops</span>
                </div>
                <div class="box">
                    <span><b>26.</b> Interior Design and Furniture Workshops</span>
                </div>
                <div class="box">
                    <span><b>27.</b> Window and Metal – Frame Production</span>
                </div>

                
            </div>
        </div>

@endsection