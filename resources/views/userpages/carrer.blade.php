@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content')

 <!-- nav end -->
<div class="carrer-container">
        <div class="inn-container">
            <h2>{{__('carrer.Title')}}</h2>
            <h2>{{__('carrer.Title1')}}</h2>
            <h2>{{__('carrer.Title2')}}</h2>


            <ul>
                <h3>{{__('carrer.SubTitle')}}</h3>
                <li>{{__('carrer.Details1')}} </li>
                <li>{{__('carrer.Details1-1')}} </li>
                <li>{{__('carrer.Details1-2')}} </li>
                <li>{{__('carrer.Details1-3')}} </li>
            </ul>

            <ul>
                <h3>{{__('carrer.SubTitle2')}} </h3>
                <li>{{__('carrer.Details2')}} </li>
                <li>{{__('carrer.Details2-1')}} </li>
                <li>{{__('carrer.Details2-2')}} </li>
                <li>{{__('carrer.Details2-3')}} </li>
                <li>{{__('carrer.Details2-4')}} </li>
                <li>{{__('carrer.Details2-5')}} </li>
                
            </ul>

        </div>


        <div class="contactForm">
            <h3><li>{{__('carrer.msgtxt')}} </li></h3>
            <div class="inputBox">
                <input type="text" placeholder="Name">
            </div>            
            <div class="inputBox">
                <input type="text" placeholder="Email">
            </div>
            <div class="inputBox">
                <input type="text" placeholder="Telephone">
            </div>
            <div class="inputBox">
                <textarea placeholder="Message"></textarea>
            </div>
            <div class="inputBox">
                <input type="submit" value="Send">
            </div>
        </div>
    </div>
@endsection