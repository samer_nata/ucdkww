@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content')

    <div class="abcontainer">
        <img src="{{asset('images/enviroment.jpg')}}" alt="">

        <div class="about-details-container">
            <div class="EnvBoxTitle">
                <h3>{{__('enviroment.Title')}}</h3>
            </div>
            <div class="env-container">
                <div>
                    <h4>{{__('enviroment.SubTitle1')}}</h4>
                    <div class="box">{{__('enviroment.Details1')}}</div>
                
                </div>
                <div >
                    <h4>{{__('enviroment.SubTitle2')}}</h4>
                    <div class="box">{{__('enviroment.Details2')}}</div>
                </div>
                <div >
                    <h4>{{__('enviroment.SubTitle3')}}</h4>
                    <div class="box">{{__('enviroment.Details3')}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection