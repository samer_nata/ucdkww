@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])

@section('content')


</nav>
<section id="header-sec">
    <div dir="ltr" class="swiper mySwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="{{asset('images/img-3.jpg')}}" alt="">
                <div class="cover"></div>
                <div class="content active">
                    <div class="titlebox">
                        <div class="title">
                            <div class="title-wrapper">
                                <span>{{__('main.Title')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="subTitle">
                        <div class="subTitle-wrapper">
                            <span>{{__('main.SubTitle1')}} <br>{{__('main.SubTitle1-1')}} </span>

                        </div>
                    </div>
                    <div class="btn-container">
                        <div class="btn-wrapper">
                            <button class="pr-btn">{{__('main.button_txt')}} </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <img src="{{asset('images/img-4.jpg')}}" alt="">
                <div class="cover"></div>
                <div class="content">
                <div class="titlebox">
                        <div class="title">
                            <div class="title-wrapper">
                                <span>{{__('main.Title2')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="subTitle">
                        <div class="subTitle-wrapper">
                            <span>{{__('main.SubTitle2')}} <br>{{__('main.SubTitle2-2')}} </span>

                        </div>
                    </div>
                    <div class="btn-container">
                        <div class="btn-wrapper">
                            <button class="pr-btn">{{__('main.button_txt')}} </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <img src="{{asset('images/img-5.jpg')}}" alt="">
                <div class="cover"></div>
                <div class="content">
                <div class="titlebox">
                        <div class="title">
                            <div class="title-wrapper">
                                <span>{{__('main.Title')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="subTitle">
                        <div class="subTitle-wrapper">
                            <span>{{__('main.SubTitle3')}} <br>{{__('main.SubTitle3-3')}} </span>

                        </div>
                    </div>
                    <div class="btn-container">
                        <div class="btn-wrapper">
                            <button class="pr-btn">{{__('main.button_txt')}} </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <img src="{{asset('images/img-6.jpg')}}" alt="">
                <div class="cover"></div>
                <div class="content">
                <div class="titlebox">
                        <div class="title">
                            <div class="title-wrapper">
                                <span>{{__('main.Title')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="subTitle">
                        <div class="subTitle-wrapper">
                            <span>{{__('main.SubTitle4')}} <br>{{__('main.SubTitle4-4')}} </span>

                        </div>
                    </div>
                    <div class="btn-container">
                        <div class="btn-wrapper">
                            <button class="pr-btn">{{__('main.button_txt')}} </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Category start -->
<section id="category-sec">

    <div class="category-cont">
        <div class="card">
            <img src="{{asset('images/oil and gas.jpg')}}" alt="" class="cardImg">
            <div class="cover"></div>

            <div class="content">
                <h2 class="title">{{__('main.Title')}}</h2>
                <button class="details-btn">{{__('main.button_txt2')}}</button>
            </div>
        </div>
        <div class="card">
            <div class="cover"></div>

            <img src="{{asset('images/construction.jpg')}}" alt="" class="cardImg">
            <div class="content">
                <h2 class="title">{{__('main.Title2')}}</h2>
                <button class="details-btn">{{__('main.button_txt2')}}</button>
            </div>
        </div>

    </div>
</section>
<!-- Category end -->

@endsection
