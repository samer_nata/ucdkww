@extends('layouts.master',['cats'=>$cats,'infs'=>$infs])
@section('content') 
    <!-- Info Section start -->
    <section class="map-container">
            <x-mapbox id="mapId" 
            :navigationControls="true" 
            :center="['long' => 47.97629445817659, 'lat'=>29.372278351591625]"
             :markers="[['long' => 47.97629445817659,'lat' => 29.372278351591625,'description' => 'برج الحمام' ]]" />
            
            <div class="contact-sec">
                <h1>Address</h1>
                    <div class="loc">
                        <h2>Beriut</h2>
                        <ul class="details">
                            <li>
                                <span><i class=" fas fa-map-marker-alt"></i></span>
                                <span><b>Beirut, Headquarter </b>- Beirut, Raouche, Raouche Street, Bahri Gardens Buidling Block 2, 13th Floor, Unit 20/1086 Ras Beirut</span>
                            </li>
                            <li>
                                <span><i class=" fas fa-phone-alt"></i></span>
                                <p><a href="#"><b>Tel/Fax:</b> +9611802311 - +9611802322</a><br>
                                </p>
                            </li>
                        </ul>
                        
                    </div>
                    <div class="loc">
                        <h2>Iraq</h2>
                        <ul class="details">
                            <li>
                                <span><i class=" fas fa-map-marker-alt"></i></span>
                                <span><b>Baghdad</b> - Al Karadah - Area 905 - Street 21 - UCD Bldg.</span>
                            </li>
                            <li>
                                <span><i class=" fas fa-phone-alt"></i></span>
                                <p><a href="#"><b>Tel:</b> +9647802501000</a><br>
                                </p>
                            </li>
                        </ul>
                        <hr>
                        <ul class="details">
                            <li>
                                <span><i class=" fas fa-map-marker-alt"></i></span>
                                <span><b>Basrah</b> - Al Gazaer - Hospital bin Ghazwan - UCD Bldg.</span>
                            </li>
                            <li>
                                <span><i class=" fas fa-phone-alt"></i></span>
                                <p><a href="#"><b>Mobile:</b> +9647809276944</a><br>
                                </p>
                            </li>
                        </ul>
                        
                    </div>
                <h1>Email</h1>
                <div class="loc">
                    <span><b>info@ucdoffshore.com</b></span>
                
                    
                </div>
            </div>

        </section>
        
    <!-- Info Section End -->
    <section class="contactFormCont">
            <div class="title">
                <h2 class="titleText"><span>C</span>ontact Us</h2>
            </div>
            <div class="contactForm">
                <h3>Send Message</h3>
                <div class="inputBox">
                    <input type="text" placeholder="Name">
                </div>            
                <div class="inputBox">
                    <input type="text" placeholder="Email">
                </div>
                <div class="inputBox">
                    <input type="text" placeholder="Telephone">
                </div>
                <div class="inputBox">
                    <textarea placeholder="Message"></textarea>
                </div>
                <div class="inputBox">
                    <input type="submit" value="Send">
                </div>
            </div>
    </section>
    
    <!-- footer start -->
@endsection