    
  
  if (window.location.pathname.split("/").pop() == 'ar')
  $('html').attr('dir', 'rtl');
else
  $('html').attr('dir', 'ltr');

function vertical_tabs() {
if ($('.ux-vertical-tabs').length > 0) {
  $('.ux-vertical-tabs .tabs button').on("click", function () {
      var temp_tab = $(this).data('tab');
      var temp_tab_js = this.getAttribute('data-tab');
      var temp_content = $('.ux-vertical-tabs .maincontent .tabcontent[data-tab="' + temp_tab + '"]');
      var temp_content_js = document.querySelector('.ux-vertical-tabs .maincontent .tabcontent[data-tab="' + temp_tab_js + '"]');
      var temp_content_active_js = document.querySelector('.ux-vertical-tabs .maincontent .tabcontent.active');

      $('.ux-vertical-tabs .tabs button').removeClass('active');
      $('.ux-vertical-tabs .tabs button[data-tab="' + temp_tab + '"]').addClass('active');

      var animation_start = anime({
          duration: 400,
          targets: temp_content_active_js,
          opacity: [1, 0],
          translateX: [0, '100%'],
          easing: 'easeInOutCubic',
          complete: function () {
              $('.ux-vertical-tabs .maincontent .tabcontent').removeClass('active');
              temp_content.addClass('active');
              var animation_end = anime({
                  duration: 400,
                  targets: temp_content_js,
                  opacity: [0, 1],
                  translateX: ['100%', '0'],
                  easing: 'easeInOutCubic'
              });
          }
      });
  });
}
}

$(function () {
vertical_tabs();
});


$("#lang-btn").click(function () {
if ($('html').attr('lang') == 'en') {
  $('html').attr('dir', 'rtl');
  $('html').attr('lang', 'ar');
  $("#lang-btn").attr("src", "img/en-logo.png");
} else if ($('html').attr('lang') == 'ar') {
  $('html').attr('dir', 'ltr');
  $('html').attr('lang', 'en');
  $("#lang-btn").attr("src", "img/ar-logo.png");
}

})
$('.icon').click(function () {
$('i').toggleClass("cancel");
});


// MAP API 

mapboxgl.accessToken = 'pk.eyJ1IjoibW9oYW1tYWRwaCIsImEiOiJja3VndWRoeHUwY3JoMndteWNvMHgybTJhIn0.BQ2eU7bTXTom7WpUO5WxOQ';
const map = new mapboxgl.Map({
container: 'map', // container ID
style: 'mapbox://styles/mapbox/streets-v11', // style URL
center: [35.5018,33.8938], // starting position
zoom: 10 // starting zoom
});

// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());



const swiper = new Swiper('#mySwiper', {
slidesPerView: 1,
loop: true,
effect: "fade",
autoplay: {
delay: 2500,
disableOnInteraction: false,
},
});
console.log(swiper);