<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Home" => "الصفحة الرئيسية",
    "About Us" => "لمحة عنا",
    "Our Services" => "خدماتنا",
    "Equipment" => "المعدات",
    "Carrer" => "الوظائف",
    "Contact Us" => "اتصل بنا",
    "Enviroment" => "البيئة",
    "Language" => "اللغة",
    "About Us Content" => "الشركة المتحدة للتطوير المحدودة المسؤولية هي شركة لديها أكثر من 30 عامًا من الخبرة من فريق إدارتها في مجالات النفط
    الحفر . تمتلك الشركة محفظة بملايين الدولارات من
    المشاريع المنفذة في منطقة الشرق الأوسط وأفريقيا" ,
    "Quick Links" => "روابط سريعة",
    "Contact Info" =>"معلومات الاتصال",
    "Contact Details" => "الطابق الرابع - البناء 732 ",
    "Contact Details2" => "فيردون , بيروت و لبنان",
   



];
