<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Title" => "النغط والغاز",
    "Title2"=> "أعمال البناء",
    "SubTitle1" => "تمكنت الشركة المتحدة للتطوير من الركوب ",
    "SubTitle1-1" => " على القمة في صناعة النفط والغاز",
    "SubTitle2" =>"تقدر الشركة المتحدة للتطوير من قيمة ",
    "SubTitle2-2" =>"اندماج تخطيط النقل  مع النصاميم العربية",
    "SubTitle3" =>"قمنا بعقد شراكة مع كبار البائعين حول العالم",
    "SubTitle3-3" =>"لتوفير حلول حفر شاملة  ",
    "SubTitle4" =>"يراقب مهندسو الشركة المتحدة للتطوير  ",
    "SubTitle4-4" =>"لاستدامة البيئة وحيويتها",
    "button_txt" => "تحقق من ذلك",
    "button_txt2" => "تفاصيل"

];
