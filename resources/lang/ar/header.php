<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Home" => "الصفحة الرئيسية",
    "About Us" => "لمحة عنا",
    "Our Services" => "خدماتنا",
    "Equipment" => "المعدات",
    "Carrer" => "الوظائف",
    "Contact Us" => "اتصل بنا",
    "Language" => "اللغة",
    "OilAndGas" => "النفط والغاز",
    "Construction" => "أعمال البناء",
    "profile" => "الملف الشخصي"

];
