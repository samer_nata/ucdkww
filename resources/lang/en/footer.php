<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Home" => "Home",
    "About Us" => "About Us",
    "Our Services" => "Our Services",
    "Equipment" => "Equipment",
    "Carrer" => "Carrer",
    "Contact Us" => "Contact Us",
    "Language" => "Language",
    "Enviroment" => "Enviroment",
    "About Us Content" => "UCD is a company with over 30 years of experience of its management team in the areas of oil drilling The company has a multi-million dollar portfolio of
    projects executed in the Middle East and Africa" ,
    "Quick Links" => "Quick Links",
    "Contact Info" =>"Contact Info",
    "Contact Details" => "4th Floor - 732 Building " ,
    "Contact Details2" => "Verdun, Beirut, Lebanon",



];
