<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "About_us" => "About Us ",
    "abText1" => "UCD is a company with over 30 years of experience of its management team in the areas of oil drilling, construction and contracting. The company has a multi-million dollar portfolio of projects executed in the Middle East and Africa.",
    "abText2" => "UCD ensures a consistent quality of work for all clients in the public or private sector. The wide spectrum of capabilities and experience is beneficial for the engagement and execution of high quality projects by delivering buildings through construction services and efficient development of oil well drilling project.",
    "abText3" => "This has resulted in satisfied clients from the public and private sectors persistence in employing UCD and gain from its dedication and services.",
    "abText4" => "UCD performs strategic joint ventures with a team of skilled engineers and staff for full fledge project management.",
    "abText5" => "UCD firmly believes in its obligation towards the environment. This results in a full commitment towards limiting possible environmental impacts by our operations.",
    "Misson" => "Mission",
    "MText1" => "1-Expand and develop while ensuring high quality standards and customized services",
    "MText2" => "2-Respect sustainability by ensuring safety and preserving the environment",
    "MText3" => "3-Innovate ourselves by providing up to date services that guarantees a trustful relationship with the clients",
    "vision" => "Vision",
    "vText1" => "UCD’s vision is to maintain its success and becoming a leader in the construction, contracting and oil drilling. ",

    



];
