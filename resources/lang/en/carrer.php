<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Title" => "Interior Designer",
    "Title1" =>"Architect",
    "Title2" => "Operations Manager",
    "SubTitle" => "Job Description",
    "Details1" => "Bachelor of Engineering (Civil) ",
    "Details1-1"=>"Minimum of 10 years of experience in the construction field and not less than 5 years of relevant experience as described in the position.",
    "Details1-2"=>"Leading all project operations in civil works, infrastructure, earth development and refurbishment.",

    "Details1-3"=>"Master degree and Gulf experience are preferred.",

    "SubTitle2" => "Desired Skills",
    "Details2" => "Good communication skills",
    "Details2-1" =>"Be able to prioritize projects",
    "Details2-2" => "Be able to work under pressure",
    "Details2-3" => "Excellent English communication skills (verbal/written)",
    "Details2-4" => "Interpersonal and leadership skills",
    "Details2-5" => "Track operational progress closely and generate required reports in a timely manner",

    "msgtxt" => "Send Message" ,





];
