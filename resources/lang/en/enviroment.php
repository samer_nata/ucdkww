<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Title" => "At UCD we highly believe in giving back to our environment, staff and stakeholders. That’s why we have developed a set of policies and procedures respective to our company:",
    "SubTitle1" => "Anti-Corruption",
    "Details1" => "In order to deliver high quality projects with precision, UCD offshore encourages an anti-corruption policy as part of its effective and efficient plan to deliver frequent and trusted services",

    "SubTitle2" => "Training and Development",
    "Details2" => "At UCD we consider our staff a valuable asset and resource that we constantly want to reinvent to produce optimum performance. In order to reproduce their skills and encourage their efforts, UCD conducts yearly evaluation programs and training seminars respective to each department.",
    
    "SubTitle3" => "The Environment",
    "Details3" => "Natural Earth’s Resources is endangered knowing that it has been vigorously exhausted throughout the years. This has urged UCD to formulate a ‘Green Policy’ as part of giving back to nature. We ensure minimizing pollution and liquid fallbacks. In addition, ‘Construction Waste Recycling’ is part of our green policy in which we separate and recycle recoverable waste materials that are generated during construction. Waste recycling is also performed in renovation.",





];
