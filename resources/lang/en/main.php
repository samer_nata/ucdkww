<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    "Title" => "Oil And Gas",
    "Title2"=> "Construction",
    "SubTitle1" => "UCD has managed to ride on the top ",
    "SubTitle1-1" => " of Oil and Gas industry",
    "SubTitle2" =>"UCD values the fusion of transportation planning ",
    "SubTitle2-2" =>"with urban design",
    "SubTitle3" =>"We have partnered with the world’s top vendors",
    "SubTitle3-3" =>"to provide comprehensive drilling solutions ",
    "SubTitle4" =>"UCD ‘s engineers keeps an eye for the sustainability",
    "SubTitle4-4" =>"and vibrancy of the environment",
    "button_txt" => "Check Out",
    "button_txt2" => "Details"

];
